~~~
* Data types (How Python stores data)
  * Numbers: `int`, `float`, `complex`
    * `int` stores pure integers
    * `float` stores anything with a decimal
    * `complex` stores complex numbers, but you probably won't have to worry about that unless you do amazing & fancy math that I can't help you with
    * You can force a variable to be a certain type by writing something like this:
    ```
    > i = int(3.14)
    > print(i)
    3
    > j = float(i)
    > print(j)
    3.0
    ```
    * Cool note: You can denote "infinity" in python by writing `float('inf')`
  * Strings
    * String of characters
    * `"string1"` == `'string1'`
~~~