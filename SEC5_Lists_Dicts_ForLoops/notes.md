# Module 2: Lists & Dictionaries (& the "for" loop)
* Lists
  * Stores lists of data in order
  * Can be multiple types of data  
  * Access an item by `[]`
  * Index can be negative to access by index from the end of the list
  * The first item in a list has index 0
  ```
  > list = [1, 2, 'red', 'blue', 3.14]
  > print(list[0])
  1
  > print(list[-1])
  3.14
  ```
  * Add a value to the end with the `append()` function:
  ```
  > list.append('new value')
  > print(list)
  [1, 2, 'red', 'blue', 3.14, 'new value']
  ```
  * Remove a value by index with the `pop()` function. It will return the value that it's removing, but you don't need to assign that to anything.
  ```
  > list.pop(-1)
  'new_value'
  > print(list)
  [1, 2, 'red', 'blue', 3.14]
  ```
* Dictionaries
  * Like a list, but the index can be anything
  * Key-value pairs
  * Accessed just like a list
  ```
  > dict = {
  >     'word': 'meaning',
  >     'libre': 'free as in freedom',
  >     'pi': 3.14159
  > }
  > dict[libre]
  'free as in freedom'
  ```
  * Add a value by acting like the key is already there and assigning a value:
  ```
  > dict['new key'] = 'new value'
  ```
  * Remove an entry from the dictionary by using a similar pop function to the list:
  ```
  > dict.pop('new key')
  'new value'
  ```
* For loop
  ```
  > nums = [5, 7, 9]
  > for num in nums:
  >     print(num)
  5
  7
  9  
  ```
  ```
  > for num in range(5, 9, 2):
  >     print(num)
  5
  7
  9
  ```

## Demo:
lists:
```
list = [1, 2, 3]
```
Adding to a list:
```
list.append(4)
```
Deleting from a list:
```
list.pop(-1)
```
