Lesson and Demo 1 (15mins)
------------------

1)  vi file with only 5 + 5

     - run file ( 'python <file>' and notice that it succeeds, but has nothing to return or print)
     - edit the file for '5+   5', and again for '5  +   5' and run each again, and notice how white space doesn't matter here, when not designating code block
     - Do one with leading white space, and note the error

2)  Now do it in Python REPL, and note that the P or REPL immediatly prints
    the output

3)  Do it on repl.it, and note that it also does that

4)  Go back to Python REPL, and now assign x to 5 + 5
    just put x on the next line, and see that the REPL returns the value
    (Evaluate & Print)
    
    - Note how there in no special character like $ in BASH to extract or recall var
    - Note how REPL is different than actual executed program in that regard

5)  Do the same thing in repl.it, assign 5 + 5 to var 'x', put x on 2nd line, and run

    - Note that this REPL does *NOT* print to the console, and when you hit "run", it's like a typing "python" before a command
    - Note that variables are saved within any REPL session, and you need to start new ones if you want a clean env.
    - "__ pycache __"

6)  Add print(x) line to repl.it and run

    - Explain that print is a function in P3, and functions take
      arguments in parenth
    - Talk about ':' to start code block
    - Talk more about white space (space v TAB)
    - Talk PEP 4 space std
    - Talk that def func assume a code block, so you need ':'
      but you can just "pass" as code block (implied "return")

7)  Go back to vi file, and edit it to print(x), re-run with "python <file>"

    - Note that the program now has something to do with your variable assignment.
    - Change it to 'print   (x)' and notice again that white space doesn't matter
    - Note that anything with a .py is a "module"

8)  Note the different ways to quit the Python REPL.

9)  Note that REPL is a good way to test chuncks of code real-tine, and line by line,
    but may not simulate a real Python program
    
    - Also note that REPLs have some shorthand syntax you may read about, that
      help speed coding or help test code, but it isn't real Python syntax
    - Also note that the REPL returned you the "x", but in real program, that would
      just be returned internally

LAB - HelloWorld script 
