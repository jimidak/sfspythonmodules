#! /anaconda/bin/python
class Student:
    def __init__(self, fn, ln, g, a, em):
        self.fname = fn
        self.lname = ln
        self.gender = g
        self.age = a
        self.email_address = em
        
    def whoswho(self):
        if self.lname == "willson":
            print(self.lname, "Is SFS OG")
        elif self.lname == "champion":
            print(self.lname, "Is an SFS capitan")
    
    def young(self):
        if self.age < 35:
            print(self.age, "fresh perspective")
        elif self.age > 34:
            print(self.age, "wisdom")
